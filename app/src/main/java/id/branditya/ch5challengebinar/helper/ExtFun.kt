package id.branditya.ch5challengebinar.helper

import androidx.activity.ComponentActivity
import androidx.activity.viewModels
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.text.SimpleDateFormat
import java.util.*

inline fun <reified T : ViewModel> ComponentActivity.viewModelsFactory(crossinline viewModelInitialization: () -> T): Lazy<T> {
    return viewModels {
        object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                return viewModelInitialization.invoke() as T
            }
        }
    }
}

inline fun <reified T : ViewModel> Fragment.viewModelsFactory(crossinline viewModelInitialization: () -> T): Lazy<T> {
    return viewModels {
        object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                return viewModelInitialization.invoke() as T
            }
        }
    }
}

fun String.toDate(): String? {
    if (this.isEmpty()) {
        return "-"
    }
    // pattern tanggal dari api
    val inputPattern = "yyyy-MM-dd"
    // pattern yang kita inginkan
    val outputPattern = "MMM dd, yyyy"

    val inputFormat = SimpleDateFormat(inputPattern, Locale.getDefault())
    val outputFormat = SimpleDateFormat(outputPattern, Locale("in"))

    // Parsing tanggal dari api, this itu adalah String yang di pake buat manggil toDate()
    // parsing adalah ubah tipe data string menjadi tipe data Date
    val inputDate = inputFormat.parse(this)

    // .format adalah ubah tipe data Date menjadi tipe data String
    return inputDate?.let {
        outputFormat.format(it)
    }

}

fun String.toDateDMY(): String? {
    if (this.isEmpty()) {
        return "-"
    }
    val inputPattern = "yyyy-MM-dd"
    val outputPattern = "dd/MM/yyyy"

    val inputFormat = SimpleDateFormat(inputPattern, Locale.getDefault())
    val outputFormat = SimpleDateFormat(outputPattern, Locale("in"))
    val inputDate = inputFormat.parse(this)

    return inputDate?.let {
        outputFormat.format(it)
    }
}


