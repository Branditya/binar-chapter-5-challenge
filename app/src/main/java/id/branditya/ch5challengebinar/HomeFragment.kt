package id.branditya.ch5challengebinar

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import id.branditya.ch5challengebinar.databinding.FragmentHomeBinding
import id.branditya.ch5challengebinar.adapter.MovieAdapter
import id.branditya.ch5challengebinar.databinding.LayoutDialogDetailUserBinding
import id.branditya.ch5challengebinar.helper.SharedPref
import id.branditya.ch5challengebinar.helper.viewModelsFactory
import id.branditya.ch5challengebinar.service.ApiClient
import id.branditya.ch5challengebinar.service.ApiService

class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private lateinit var movieAdapter: MovieAdapter

    private val apiService: ApiService by lazy { ApiClient.instance }

    private val viewModel: HomeViewModel by viewModelsFactory { HomeViewModel(apiService) }

    private val sharedPref: SharedPref by lazy { SharedPref(requireContext()) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        changeTvTitle()
        profileButtonClicked()
        initRecyclerView()
        viewModel.getDataFromNetwork()
        observeData()
    }

    private fun observeData() {
        viewModel.movie.observe(viewLifecycleOwner) {
            movieAdapter.updateData(it)
            binding.pbHome.visibility = View.GONE
        }
        viewModel.errorMessage.observe(viewLifecycleOwner) {
            createToast(it).show()
        }
    }

    private fun changeTvTitle() {
        val username = sharedPref.getDataUsername()
        binding.tvShowUsername.text = username
    }

    private fun profileButtonClicked() {
        binding.btnShowProfile.setOnClickListener {
            showDialogProfile()
        }
    }

    private fun showDialogProfile() {
        val binding = LayoutDialogDetailUserBinding.inflate(
            LayoutInflater.from(requireContext()),
            null,
            false
        )
        val customLayout = binding.root

        val username = sharedPref.getDataUsername()
        val email = sharedPref.getDataEmail()
        binding.tvProfileUsername.text = username
        binding.tvProfileEmail.text = email

        val builder = AlertDialog.Builder(requireContext())
        builder.setView(customLayout)
        val dialog = builder.create()
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        binding.apply {
            btnEditProfile.setOnClickListener {
                findNavController().navigate(R.id.action_homeFragment_to_profileFragment)
                dialog.dismiss()
            }
        }
        dialog.show()
    }

    private fun initRecyclerView() {
        binding.apply {
            movieAdapter = MovieAdapter {
                val movieId = it.id
                val bundle = Bundle()
                bundle.putInt("KEY_ID", movieId)
                findNavController().navigate(
                    R.id.action_homeFragment_to_detailMovieFragment,
                    bundle
                )
            }
            rvNote.adapter = movieAdapter
            rvNote.layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun createToast(message: String): Toast {
        return Toast.makeText(requireContext(), message, Toast.LENGTH_LONG)
    }
}