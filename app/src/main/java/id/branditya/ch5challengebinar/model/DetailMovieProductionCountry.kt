package id.branditya.ch5challengebinar.model


import com.google.gson.annotations.SerializedName

data class DetailMovieProductionCountry(
    @SerializedName("iso_3166_1")
    val iso31661: String,
    @SerializedName("name")
    val name: String
)