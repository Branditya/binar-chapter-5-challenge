package id.branditya.ch5challengebinar

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import id.branditya.ch5challengebinar.model.PopularMovie
import id.branditya.ch5challengebinar.model.ResultPopularMovie
import id.branditya.ch5challengebinar.service.ApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeViewModel(private val apiService: ApiService) : ViewModel() {
    private var _movies = MutableLiveData<List<ResultPopularMovie>>()
    val movie: LiveData<List<ResultPopularMovie>> get() = _movies

    private var _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String> get() = _errorMessage

    fun getDataFromNetwork() {
        apiService.getPopularMovie(BuildConfig.API_KEY).enqueue(object : Callback<PopularMovie> {
            override fun onResponse(call: Call<PopularMovie>, response: Response<PopularMovie>) {
                if (response.isSuccessful) {
                    _movies.postValue(response.body()?.resultPopularMovies)
                } else {
                    _errorMessage.postValue(response.errorBody().toString())
                }
            }

            override fun onFailure(call: Call<PopularMovie>, t: Throwable) {
                _errorMessage.postValue(t.toString())
            }
        })
    }

}